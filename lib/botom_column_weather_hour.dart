import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:provider_app/weather_model.dart';

class BottomColumnWeatherPerHour extends StatelessWidget {
  // BottomColumnWeatherPerHour(this.weatherPerHour);
  //
  // final WeatherPerHour weatherPerHour;

  @override
  Widget build(BuildContext context) {
    final weather = Provider.of<WeatherPerHour>(context);
    return Container(
      height: 136,
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xffE4F0FA),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            '${weather.temperature}°C',
          ),
          Text(
            '${weather.humidity}',
          ),
        ],
      ),
    );
  }
}
