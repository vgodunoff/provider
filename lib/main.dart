import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_app/weather_model.dart';

import 'botom_column_weather_hour.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => WeatherPerHour(temperature: 7, windSpeed: 4, humidity: 85),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryIconTheme: IconThemeData(color: Colors.white),
          primaryColor: Color(0xff47b1e6),
        ),
        home: CurrentWeatherPage(),
      ),
    );
  }
}

class CurrentWeatherPage extends StatelessWidget {
  // WeatherPerHour weatherPerHour = WeatherPerHour();
  @override
  Widget build(BuildContext context) {
    final weather = Provider.of<WeatherPerHour>(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Color(0xff47b1e6),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius:
                    BorderRadius.vertical(top: Radius.circular(24.0))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 12,
                ),
                SizedBox(
                  height: 138,
                  child: ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    children: [BottomColumnWeatherPerHour()],
                  ),
                ),
                SizedBox(
                  height: 24 + MediaQuery.of(context).viewPadding.bottom,
                ),
              ],
            ),
          ),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
