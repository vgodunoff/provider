class WeatherPerHour {
  final double temperature;
  final int windSpeed;
  final int humidity;

  WeatherPerHour({this.windSpeed, this.humidity, this.temperature});
}
